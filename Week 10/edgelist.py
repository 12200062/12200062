class Graph:
	# constructor
	def __init__(self):
		self.graph = []

	def add_edge(self, src, dest):
		self.graph.append((src, dest))

if __name__ == '__main__':
	g = Graph() # creating instance
	g.add_edge('A', 'B')
	g.add_edge('A', 'C')
	g.add_edge('B', 'C')
	g.add_edge('B', 'E')
	g.add_edge('C', 'D')
	g.add_edge('D', 'E')
	print(g.graph)
