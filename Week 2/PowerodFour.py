def PowerOfFour(n):
    if n==0:
        return False
    while(n != 1):
        if(n%4!=0):
            return False
        n = n>>2
    return True
n = 16
print(PowerOfFour(n))